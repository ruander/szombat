Weboldal szöveges tartalma

--------------------------------------------------------------------------------
TARTALOMJEGYZÉK
--------------------------------------------------------------------------------

I. Weboldal címe
II. Navigáció
III. Közösségi / Megosztás
IV. Képváltó / Welcome
V. Legfrissebb munkák / Portfolio
VI. Előnyök / Benefits
VII. Karrier / Career
VIII. Jellemzők / Features 
IX. Szolgáltatások / Services
X. Ajánlások / Testimonials
XI. Gyakran ismételt kérdések / Frequently Asked Questions
XII. Navigáció 
XIII. Partnerek / Partners
XIV. Kapcsolat / Contact
XV. Feliratkozás / Subscribe
XVI. Közösségi / Megosztás
XVII. Szerzői jog / Copyright 

--------------------------------------------------------------------------------
    
I. Weboldal címe
    iWorkshop | Webstúdió és élményműhely

--------------------------------------------------------------------------------
    
II. Navigáció [nav ul]

    Welcome [li]
    Portfolio [li]
    Benefits [li]
    Careers [li]
    Features [li]
    Services [li]
    FAQ [li]
    Contact [li]
    Offers [li dropdown form]
    
        Email address [input email] 
        Newsletters [checkbox] 
        Products [checkbox] 
        Researches [checkbox] 
        Sign me up [button submit] 

--------------------------------------------------------------------------------
    
III. Közösségi / Megosztás

    Facebook [a]
    Twitter [a]
    Pinterest [a]
    Google+ [a]
    YouTube [a]
    Dropbox [a]

--------------------------------------------------------------------------------
    
IV. Képváltó / Welcome

    This is a Carousel [heading]
    Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit
    non mi porta gravida at eget metus [caption]
    
    Mauris sodales [heading]
    Pellentesque congue, lectus nec ullamcorper volutpat, magna risus hendrerit
    erat, non mattis nibh felis nec nisi [caption]
    
    Integer imperdiet [heading]
    Donec purus leo, blandit quis dui id, blandit feugiat nunc. Sed volutpat, mi
    in tempor fermentum, lorem odio varius nunc [caption]
    
    Learn more [button]

--------------------------------------------------------------------------------
    
V. Legfrissebb munkák / Portfolio

    This is a Heading [heading]
    Donec sed est aliquam, ultricies elit porta, mattis odio. Pellentesque
    hendrerit nec ipsum quis condimentum. Pellentesque habitant morbi tristique
    senectus et netus et malesuada fames ac turpis egestas aliquet ut velit. [p]
    Learn more [button]

    Reap what you sow [heading]
    Morbi porta sem sit amet nunc consectetur, id finibus justo congue. In nisl
    ex, tincidunt nec consequat eu, hendrerit non felis. Ut ac pulvinar ex, non
    suscipit leo. [p]
    Learn more [button]

    Crank it up [heading]
    Pellentesque ante libero, efficitur et augue in, sagittis pellentesque
    nisi. Suspendisse ex justo, imperdiet eget hendrerit ac, pretium vitae mi.
    Aliquam finibus nisi nec enim placerat sagittis. Vivamus dignissim. Etiam
    tincidunt. [p]
    Learn more [button]

--------------------------------------------------------------------------------
    
VI. Előnyök / Benefits

    A heading again [heading]
    Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit
    non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies
    vehicula ut id elit. Nec ipsum quis [p]
    Try this modal [button]

        This is a modal's heading [heading]
        And some important text the website wants to share with you. Cras justo
        odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi
        porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula
        ut id elit. [p]
        Dismiss [button]
        Approve [button]

--------------------------------------------------------------------------------
    
VII. Karrier / Career

    One page design offers powerful solutions [heading]
    Quisque ultrices, dui nec ornare elementum, libero quam finibus ex, ac
    sodales justo turpis id libero. Phasellus convallis augue lorem, in rhoncus
    nibh ultricies vel. [p]
    
--------------------------------------------------------------------------------
    
VIII. Jellemzők / Features 

    Another heading [heading]
    Cras justo odio, dapibus ac facilisis in, egestas eget quam  donec. Id elit
    non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies
    vehicula ut id elit. [p]

        A smaller Heading [heading]
        Sed tincidunt nisi non turpis pharetra, vel vehicula sem tempor. Duis
        rutrum enim sit amet malesuada faucibus. Morbi vestibulum elit vitae
        erat blandit, ut tincidunt nunc mollis. [p]
        Learn more [button]

        Double power [heading]
        Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.
        Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus,
        porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus
        magna. [p]
        Learn more [button]

--------------------------------------------------------------------------------
    
IX. Szolgáltatások / Services

    Here comes some text [heading]

    Tempus [heading]
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nulla nulla,
    finibus eget lorem in, dignissim vestibulum arcu. Ut pharetra, dui nec
    feugiat efficitur, turpis ligula sodales mi, in malesuada diam magna eu
    augue. Phasellus venenatis justo eleifend est volutpat, nec semper libero
    aliquam. Sed elit ligula, posuere ut condimentum non, porta ac turpis. [p]
    Integer vel facilisis metus. Sed pretium sapien vitae eros scelerisque
    faucibus. In viverra id magna sed ultricies. Vestibulum ornare fringilla
    lectus rutrum mattis. Donec maximus mauris a tellus varius gravida. Maecenas
    et mi a leo egestas vestibulum sit amet vitae nisi. Praesent sit amet
    imperdiet quam. Etiam arcu dui, molestie eu nibh et, sollicitudin iaculis
    ligula. Praesent vitae odio auctor, laoreet diam eget, varius augue. [p]

    Auctor [heading]
    In hac habitasse platea dictumst. Donec at felis vitae ex vulputate maximus.
    Ut non convallis leo. Pellentesque ut nibh ut metus ultricies cursus. Nulla
    sagittis elit sodales, luctus orci non, aliquet lectus. Vestibulum interdum
    maximus nisl id tincidunt. Sed eu sem id ante gravida placerat a non felis.
    Sed vestibulum in odio sit amet dignissim. In dignissim dui mauris, eu
    sagittis sem aliquam at. [p]
    Donec condimentum feugiat fermentum. Cras a condimentum lorem. Proin sed
    sagittis urna, vel rhoncus urna. Vestibulum et mauris posuere, placerat
    nulla non, fermentum arcu. Phasellus imperdiet magna tellus, vel faucibus
    sapien ullamcorper eu. Phasellus egestas id diam ut faucibus. Nunc eget diam
    id tortor egestas lacinia id in erat. Vivamus vitae leo sit amet est
    molestie efficitur. [p]

    Quisque [heading]
    Nunc lacus ipsum, eleifend et felis a, egestas mollis ligula. Donec id elit
    non nibh gravida vehicula. Donec et nulla a nisl malesuada luctus. Fusce in
    nunc orci. Nam et justo finibus, feugiat purus quis, tempus orci. Sed
    bibendum enim sed nulla iaculis, vitae pharetra enim elementum. Sed pretium
    tempor mi sit amet tincidunt. [p]
    Aliquam id vulputate risus. Lorem ipsum dolor sit amet, consectetur
    adipiscing elit. Aliquam erat volutpat. Nam aliquet ultricies elit, quis
    sagittis risus hendrerit in. Quisque lacinia diam vitae maximus bibendum.
    Praesent fringilla est in dui laoreet, quis pretium est elementum. Nam nec
    massa non nibh ultrices egestas. Proin a elit semper, elementum quam vitae,
    pellentesque tortor. Phasellus turpis tortor, lacinia ut turpis tristique,
    gravida ornare risus. Duis eros velit, tempus at dui ac, fringilla interdum
    nunc. [p]

    Praesent [heading]
    Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pharetra
    erat mi, in accumsan nisi porttitor egestas. Phasellus et nisi eu dolor
    viverra placerat. Nam vehicula elementum tempus. Mauris metus justo, dapibus
    sit amet sodales sit amet, egestas non erat. Nulla sed lacus sit amet augue
    venenatis porttitor sed vel sem. Aliquam lobortis justo nec porttitor
    hendrerit. Nunc rutrum, nunc sed vehicula tincidunt, purus leo pretium
    tortor, id varius arcu nunc et dolor. Sed aliquet erat vitae facilisis
    rutrum. [p]
    Etiam tempus velit ligula, vel placerat lectus semper sed. Proin congue
    massa mauris, eget scelerisque magna rutrum et. Maecenas at venenatis risus,
    non eleifend odio. Nunc rutrum ipsum nunc, id volutpat leo tempus ac. Nullam
    viverra ex et eros volutpat rhoncus. Nulla ac vulputate velit, at tincidunt
    leo. [p]

--------------------------------------------------------------------------------
    
X. Ajánlások / Testimonials

    Testiomonials [heading]

    Sed non orci egestas, facilisis massa in, tincidunt ligula. Etiam
    pellentesque, sapien vel finibus venenatis, purus dui placerat nunc, a
    luctus mauris metus accumsan tortor. Fusce vel blandit nisi, non bibendum
    ante. Sed sed diam lorem. [blockquote]
    John Doe [heading]
    Customer [p]

    Ut sodales dolor quam, ac efficitur mi aliquam eget. In odio augue,
    scelerisque et tincidunt vitae, condimentum sed ipsum. In ornare, justo sit
    amet fermentum venenatis, lorem enim sodales tortor, ornare elementum velit
    purus quis justo. [blockquote]
    Jane Doe [heading]
    His sister [p]

    Pellentesque vulputate efficitur lectus nec accumsan. Sed tristique finibus
    velit vitae pharetra. Phasellus suscipit lectus at diam facilisis feugiat
    vel ac sem. [blockquote]
    David Smith [heading]
    Not Doe at all [p]

--------------------------------------------------------------------------------
    
XI. Gyakran ismételt kérdések / Frequently Asked Questions

    Morbi ipsum libero, auctor id laoreet? [heading]
    Morbi nulla orci, sagittis nec lorem sagittis, egestas convallis dui.
    Suspendisse quis vulputate mauris, vel iaculis nulla. Aenean at erat id enim
    posuere placerat vel vitae justo. Sed eros libero, porta nec volutpat ac,
    placerat pulvinar ex. [p]

    Cras justo odio, dapibus ac facilisis? [heading]
    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
    cubilia Curae; In porta sollicitudin tincidunt. Mauris porttitor, leo ac
    dapibus pellentesque, dui augue gravida nulla, sed porttitor sem orci
    in risus. Praesent mi sapien, tempor nec facilisis sed, ultricies eget
    orci. [p]

    Gravida at eget metus nullam? [heading]
    Proin quis est odio. Quisque gravida tellus eu sem mollis cursus. Mauris
    placerat metus quis augue tristique, nec dignissim ante posuere. Proin
    placerat varius vestibulum. In in lorem at erat vulputate vulputate. Quisque
    a facilisis ante. [p]

    Donec id elit non mi porta gravida at eget metus? [heading]
    Vivamus convallis sem vel turpis venenatis porttitor. Suspendisse non
    iaculis lorem. Pellentesque congue lorem in hendrerit euismod. Fusce
    blandit est in est laoreet, sed finibus mi commodo. Aliquam consectetur
    non ipsum nec finibus. Nunc sapien sem, faucibus at molestie a, dapibus
    sed elit. [p]

    Fusce suscipit sollicitudin scelerisque? [heading]
    Nunc gravida, lorem at consectetur finibus, quam nulla tincidunt augue, in
    viverra nisl eros a lectus. Proin suscipit sapien at lacinia posuere. Proin
    vitae tincidunt tortor. Integer eget vehicula magna. Nullam ultricies nulla
    in pellentesque finibus. [p]

    Sed consequat semper lorem quis consectetur? [heading]
    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
    ridiculus mus. Praesent ex lacus, facilisis ut tortor nec, pharetra rutrum
    risus. Nulla lacinia laoreet libero et vulputate. [p]
    
--------------------------------------------------------------------------------
    
XII. Navigáció [nav ul]

    Welcome [li]
    Portfolio [li]
    Benefits [li]
    Careers [li]
    Features [li]
    Services [li]
    FAQ [li]
    Contact [li]    

--------------------------------------------------------------------------------
    
XIII. Partnerek / Partners

    eLegant [a]
    mono.IT [a]
    examps [a]
    LOGOExample [a]
    Dinamic [a]
    Trad E-MARKeT [a]
    des:gnal [a]

--------------------------------------------------------------------------------
    
XIV. Kapcsolat / Contact

    Google Maps [iframe]

    Contact [heading]
    Studio 103 
    The Royal Palace
    12 Yellow Brick Road
    NW1 5LR Emerald City
    Boogie Wonderland  [p]

    Phone: +44 123 456 7890
    Email: example@wonderland.com [p]

--------------------------------------------------------------------------------
    
XV. Feliratkozás / Subscribe

    Subscribe [heading]
    
    Email address [input email] 
    Newsletters [checkbox] 
    Products [checkbox] 
    Researches [checkbox] 
    Sign me up [button submit] 

--------------------------------------------------------------------------------
    
XVI. Közösségi / Megosztás

    Facebook [a]
    Twitter [a]
    Pinterest [a]
    Google+ [a]
    YouTube [a]
    Dropbox [a]

--------------------------------------------------------------------------------
    
XVII. Szerzői jog / Copyright 

    2015 © iWorkshop ALL Rights Reserved. [p]
    Privacy Policy [a]
    Terms of Service [a]